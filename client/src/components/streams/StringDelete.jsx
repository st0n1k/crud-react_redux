import React from 'react';
import Modal from '../Modal';
import history from '../../history';
import {getStream, deleteStream} from '../../actions';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom';

class StreamDelete extends React.Component {
    componentDidMount() {
        console.log(this.props);
        this.props.getStream(this.props.match.params.id)
    }

    onDelete = () => {
        console.log(this.props);
        this.props.deleteStream(this.props.match.params.id);
    }
    renderActions() {
        return (
            <React.Fragment>
                <button onClick={this.onDelete} className="ui button negative">Delete</button>
                <Link to='/' className="ui button">Cancel</Link>
            </React.Fragment>
        )
    }

    renderAnswer() {
        if(!this.props.streamDelete) {
            return 'Are you sure you want to delete this stream?'
        }
        return `Are you sure you want to delete stream ${this.props.streamDelete.title}?`
    }
    
    render() {
        return (
            <Modal
                title="Delete Stream" 
                content={this.renderAnswer()}
                actions={this.renderActions()}
                onDismiss={() => history.push('/')} />
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        streamDelete: state.streams[ownProps.match.params.id]
    }
}

export default connect(mapStateToProps, {deleteStream, getStream})(StreamDelete);